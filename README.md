# README #
-----------------------------
This is a compiler for a programming language called "Cool". "Cool" is a object-oriented programming language.

The project is still in progress. Until now, there is a recursive-descent parser, a symbol table builder and a type checker.

Working on the back-end part right now.